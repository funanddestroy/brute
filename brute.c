#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <netdb.h>
#include <fcntl.h>
#include <inttypes.h>
#include <string.h>
#define __USE_GNU
#include <crypt.h>
#include <stdbool.h>
#include <pthread.h>
#include <semaphore.h>


#define PORT          3425
#define ALPH1         "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
#define ALPH          "exzasoct"
#define MAX_LEN       9
//xaoc
#define HASH          "puYeAQxEBqDwA"
#define QUEUE_SIZE    4
#define SEND_SIZE     10
#define PREFIX_SIZE   2
#define PASS_LEN      11
#define ALPH_LEN      70
#define HASH_LEN      15

#define SEND_JOB_TMPL ""			\
  "<msg>"					\
  "<type>MT_SEND_JOB</type>"			\
  "<args>"					\
  "<job>"					\
  "<job>"					\
  "<password>%s\n"				\
  "</password>"					\
  "<id>%d</id>"					\
  "<idx>%d</idx>"				\
  "<hash>%s\n"					\
  "</hash>"					\
  "<alphabet>%s\n"				\
  "</alphabet>"					\
  "<from>%d</from>"				\
  "<to>%d</to>"					\
  "</job>"					\
  "</job>"					\
  "</args>"					\
  "</msg>"

#define SEND_REPORT ""				\
  "<msg>"					\
  "<type>MT_SEND_REPORT</type>"			\
  "<args>"					\
  "<job>"					\
  "<job>"					\
  "<resault>%s\n"				\
  "</resault>"					\
  "<is_found>%d</is_found>"	       		\
  "<id>%d</id>"					\
  "<idx>%d</idx>"				\
  "</job>"					\
  "</job>"					\
  "</args>"					\
  "</msg>"
  

typedef struct send_t {
  char password[PASS_LEN];
  char res_pass[PASS_LEN];
  char alph[ALPH_LEN];
  char hash[HASH_LEN];
  int32_t from;
  int32_t to;
  int32_t res_is_found;
} send_t;

typedef enum status_t {
  ST_SUCCESS,
  ST_FAILURE,	
} status_t;

typedef char password_t[MAX_LEN + 1];

typedef struct task_t {
  password_t password;
  int from;
  int to;
  int id;
  int idx;
} task_t;

typedef struct queue_t {
  task_t tasks[QUEUE_SIZE];
  int head, tail;
  pthread_mutex_t head_mutex, tail_mutex;
  sem_t full, empty;
} queue_t;

typedef struct int_queue_t {
  int slots[QUEUE_SIZE];
  int head, tail;
  pthread_mutex_t head_mutex, tail_mutex;
  sem_t full, empty;
} int_queue_t;

typedef struct registry_t {
  task_t tasks[QUEUE_SIZE];
  bool used[QUEUE_SIZE];
  int_queue_t empty_slots;
}

typedef enum brute_mode_t {
  BM_ITER,
  BM_REC,
} brute_mode_t;

typedef enum run_mode_t {
  RM_SINGLE,
  RM_MULTI,  
  RM_SYNC_SERVER,
  RM_SYNC_CLIENT,
  RM_ASYNC_SERVER,
  RM_ASYNC_CLIENT,
} run_mode_t;

typedef struct result_t {
  pthread_mutex_t mutex;
  volatile bool is_found;
  password_t password;
} result_t;

typedef struct config_t {
  char * alph;
  int password_length;
  brute_mode_t brute_mode;
  char * hash;
  run_mode_t run_mode;
} config_t;

typedef struct context_t {
  config_t * config;
  int alph_length;
  queue_t queue;
  volatile int tasks_in_progress;
  pthread_cond_t sigsem;
  result_t result;
  pthread_mutex_t task_mutex;
  int connected_socket;
  sem_t lock_start;
} context_t;

typedef bool (*handler_t) (context_t * context, task_t * task, struct crypt_data * cd);

void queue_init (queue_t * queue) {
  queue->head = 0;
  queue->tail = 0;
  pthread_mutex_init (&queue->head_mutex, NULL);
  pthread_mutex_init (&queue->tail_mutex, NULL);
  sem_init (&queue->full, 0, 0);
  sem_init (&queue->empty, 0, QUEUE_SIZE);
}

void queue_push (queue_t * queue, task_t * pass) {
  sem_wait (&queue->empty);
  pthread_mutex_lock (&queue->tail_mutex);
  queue->tasks[queue->tail] = *pass;
  if (++queue->tail == QUEUE_SIZE)
    queue->tail = 0;
  pthread_mutex_unlock (&queue->tail_mutex);
  sem_post (&queue->full);
}

void queue_pop (queue_t * queue, task_t * task) {
  sem_wait (&queue->full);
  pthread_mutex_lock (&queue->head_mutex);
  *task = queue->tasks[queue->head];
  if (++queue->head == QUEUE_SIZE)
    queue->head = 0;
  pthread_mutex_unlock (&queue->head_mutex);
  sem_post (&queue->empty);
}

void int_queue_init (queue_t * queue) {
  queue->head = 0;
  queue->tail = 0;
  pthread_mutex_init (&queue->head_mutex, NULL);
  pthread_mutex_init (&queue->tail_mutex, NULL);
  sem_init (&queue->full, 0, 0);
  sem_init (&queue->empty, 0, QUEUE_SIZE);
}

void int_queue_push (queue_t * queue, int slot) {
  sem_wait (&queue->empty);
  pthread_mutex_lock (&queue->tail_mutex);
  queue->slots[queue->tail] = slot;
  if (++queue->tail == QUEUE_SIZE)
    queue->tail = 0;
  pthread_mutex_unlock (&queue->tail_mutex);
  sem_post (&queue->full);
}

void int_queue_pop (queue_t * queue, int * slot) {
  sem_wait (&queue->full);
  pthread_mutex_lock (&queue->head_mutex);
  *slot = queue->slots[queue->head];
  if (++queue->head == QUEUE_SIZE)
    queue->head = 0;
  pthread_mutex_unlock (&queue->head_mutex);
  sem_post (&queue->empty);
}

void registry_init (registry_t * registry) {
  int_queue_init (&registry->empty_slots);
  int i;
  for (i = 0; i < QUEUE_SIZE; i++) {
    int_queue_push (i);
  }
}

void regisrty_add_task (registry_t * registry, task_t * task) {
  int slot;
  int_queue_pop (&registry->empty_slots, &slot);
  registry->used[slot] = true;
  registry->tasks[slot] = *task;
}

void registry_remove_task (registry_t * registry, int slot) {
  int_queue_push (&registry->empty_slots, slot);
  used[slot] = false;
}

int registry_get_id (registry_t * registry, int idx) {
  return registry->tasks[idx]->id;
} 

bool check_password (context_t * context, task_t * task, struct crypt_data * cd) {
  bool matched = (!strcmp (context->config->hash, crypt_r(task->password, context->config->hash, cd)));
  if (matched) {
    strcpy (context->result.password, task->password);
    context->result.is_found = true;   
  }
  return (context->result.is_found);
}

void brute_rec (context_t * context, task_t * task, handler_t handler, struct crypt_data * cd) {
  int i;
  for (i = task->from; i < task->to; ++i) {
    task->password[i] = context->config->alph[0];
  }

  bool rec (int k) {
    int i;
    if (k == task->to) {
      return (handler (context, task, cd));
    } else {
      for (i = 0; i < context->alph_length; ++i) {
	task->password[k] = context->config->alph[i];
	if (rec (k + 1)) {
	  return (true);
	}
      }
    }
    return (false);
  }
  rec (task->from);
}

void brute_iter (context_t * context, task_t * task, handler_t handler, struct crypt_data * cd) {  
  int idx[task->to];
  int i;  
  for (i = 0; i < task->to; ++i) {
    idx[i] = 0;
    task->password[i] = context->config->alph[idx[i]];
  }  
  for (;;) {
    if (handler (context, task, cd)) {
      break;
    }
    
    for (i = task->to - 1; (idx[i] == (context->alph_length - 1)) && (i >= task->from); i--) {
      idx[i] = 0;
      task->password[i] = context->config->alph[idx[i]];
    }
    if (i < task->from) {
      break;
    }    
    task->password[i] = context->config->alph[++idx[i]];
  }
}

void brute_all (context_t * context, handler_t handler, int pos) {
  int pass_length;
  struct crypt_data cd = {
    .initialized = 0
  };
  task_t task;
  task.from = pos;

  for (pass_length = pos; pass_length <= context->config->password_length; pass_length++) {
    task.to = pass_length;
    task.password[task.to] = 0;
    switch (context->config->brute_mode) {
    case BM_ITER:      
      brute_iter (context, &task, handler, &cd);
      break;
    case BM_REC:    
      brute_rec (context, &task, handler, &cd);
      break;
    }
  }
}

void * consumer (void * arg) {
  context_t * context = arg;
  struct crypt_data cd = { 
    .initialized = 0 
  };
  for (;;) {
    task_t task;
    queue_pop (&context->queue, &task);    
    task.to = task.from;
    task.from = 0;
    
    switch (context->config->brute_mode) {
    case BM_ITER:
      brute_iter (context, &task, check_password, &cd);
      break;
    case BM_REC:
      brute_rec (context, &task, check_password, &cd);
      break;
    }
    pthread_mutex_lock(&context->task_mutex);
    if (--context->tasks_in_progress == 0) {
      pthread_cond_signal (&context->sigsem);
    }
    pthread_mutex_unlock(&context->task_mutex);
  }
}

status_t send_msg (int sock, char * buf) {
  int32_t msg_size = strlen(buf) + 1;
  int nbytes = 0;
  int size;   
    
  for (size = 0; size < sizeof (msg_size); size += nbytes) {
    nbytes = send (sock, (char*)&msg_size + size, sizeof(msg_size) - size, 0); 
    if(nbytes < 0) {
      return (ST_FAILURE);
    } 
  }  

  nbytes = 0;    
  for (size = 0; size < msg_size; size += nbytes) {
    nbytes = send (sock, (char*)buf + size, msg_size - size, 0); 
    if(nbytes < 0) {
      return (ST_FAILURE); 
    } 
  }    
  return (ST_SUCCESS);
}

status_t read_msg (int sock, char * buf) {
  int32_t msg_size;
  int nbytes = 0;
  int size;    

  for (size = 0; size < sizeof (msg_size); size += nbytes) {
    nbytes = recv (sock, (char*)&msg_size + size, sizeof(msg_size) - size, 0); 
    if (nbytes < 0) {
      return (ST_FAILURE);
    } 
  } 

  nbytes = 0;    
  for (size = 0; size < msg_size; size += nbytes) {
    nbytes = recv (sock, (char*)buf + size, msg_size - size, 0); 
    if (nbytes < 0) {
      return (ST_FAILURE);
    }
  }
  return (ST_SUCCESS);
}

void * client (void * arg) {
  context_t * context = arg;
  int connected_socket = context->connected_socket;
  sem_post (&context->lock_start);
  char send_buf[sizeof (SEND_JOB_TMPL) + 256];
  char read_buf[sizeof (SEND_REPORT) + 256];
  
  for (;;) {
    task_t task;    

    int received_is_found;
    password_t received_password;

    queue_pop (&context->queue, &task);  
    //fprintf (stderr, "pass = %s\nfrom = %d\nto = %d\n", task.password, task.from, task.to);
    
    sprintf (send_buf, SEND_JOB_TMPL, task.password, 0, 0, context->config->hash, context->config->alph, 0, task.from);
    
    if (ST_FAILURE == send_msg (connected_socket, send_buf)) {
      fprintf (stderr, "send fault\n");
    }

    if (ST_FAILURE == read_msg (connected_socket, read_buf)) {
      fprintf (stderr, "read fault\n");
    } 
    int id, idx;
    if (4 != sscanf (read_buf, SEND_REPORT, received_password, &received_is_found, &id, &idx)) {
      fprintf (stderr, "parsing fault\n");
    }  

    received_password[strlen (received_password)] = '\0';

    if (received_is_found) {
      context->result.is_found = received_is_found;
      strcpy(context->result.password, received_password);
    }
  }
}

bool push_task (context_t * context, task_t * task, struct crypt_data * cd) {
  pthread_mutex_lock (&context->task_mutex);
  context->tasks_in_progress++;
  pthread_mutex_unlock (&context->task_mutex);
  queue_push (&context->queue, task);
  return (context->result.is_found);
}

void * bruter (void * arg) {
  context_t * context = arg;
  brute_all (context, push_task, PREFIX_SIZE);  
  
  return (NULL);
}

void run_multi (context_t * context) {
  queue_init (&context->queue);
  int i, ncpu = (long)sysconf (_SC_NPROCESSORS_ONLN);
  pthread_t ids[ncpu];  

  pthread_mutex_init (&context->task_mutex, NULL);
  pthread_cond_init (&context->sigsem, NULL);

  for (i = 0; i < ncpu; ++i) {
    pthread_create (&ids[i], NULL, consumer, context);
  }

  brute_all (context, push_task, PREFIX_SIZE);
  pthread_mutex_lock (&context->task_mutex);
  while (context->tasks_in_progress != 0) {
    pthread_cond_wait (&context->sigsem, &context->task_mutex);
  }
  pthread_mutex_unlock (&context->task_mutex);

  
  for (i = 0; i < ncpu; ++i) {
    void * ret_val;
    pthread_cancel (ids[i]);
    pthread_join (ids[i], &ret_val);
  }
}

status_t run_sync_server_socket (context_t * context, int sock) {
  struct sockaddr_in clientname;

  clientname.sin_family = AF_INET;
  clientname.sin_port = htons(PORT);
  clientname.sin_addr.s_addr = INADDR_ANY;
  if(bind(sock, (struct sockaddr *) &clientname, sizeof(clientname)) < 0) {
    return (ST_FAILURE);
  }

  if (listen (sock, 1) < 0) {
    return (ST_FAILURE);
  }
  
  pthread_t ids;
  pthread_create (&ids, NULL, bruter, context);
  pthread_detach (ids);  

  socklen_t size;     
      
  sem_init (&context->lock_start, 0, 0);
      
  for(;;) {
    size = sizeof (clientname);
    context->connected_socket = accept (sock, (struct sockaddr*) &clientname, &size);
    //fprintf (stderr, "Server: connect from host 0x%x, port %hd.\n", clientname.sin_addr.s_addr, ntohs (clientname.sin_port));
      
    pthread_create (&ids, NULL, client, context);
    pthread_detach (ids);
    
    sem_wait (&context->lock_start);
    
    if (context->result.is_found) {
      break;
    }
  }
  return (ST_SUCCESS);
}

void * server (void * arg) {
  context_t * context = arg;
    
  int sock;
  sock = socket (AF_INET, SOCK_STREAM, 0);
  if (sock < 0) {
    return (NULL);
  }

  run_sync_server_socket (context, sock);
    
  close (sock);
  return (NULL);
}
  
status_t run_sync_server (context_t * context) {  
  queue_init (&context->queue);
  
  pthread_t ids;
  pthread_create (&ids, NULL, server, context);
  pthread_detach (ids);

  for(;;) {    
    if (context->result.is_found) {
      break;
    }
  }
    
  return (ST_SUCCESS);
}

status_t run_sync_client_socket (context_t * context, int sock) {
  struct crypt_data cd = { 
    .initialized = 0 
  };
    
  struct sockaddr_in servername;

  servername.sin_family = AF_INET;
  servername.sin_port = htons (PORT); 
  servername.sin_addr.s_addr = INADDR_ANY;
  if(connect (sock, (struct sockaddr *)&servername, sizeof (servername)) < 0) {
    return (ST_FAILURE);
  }
  
  for (;;) {
    char read_buf[sizeof (SEND_JOB_TMPL) + 256];
    char send_buf[sizeof (SEND_REPORT) + 256];

    password_t password;
    int id, idx;
    char hash[14];
    char alph[256];
    
    task_t task;

    if (ST_SUCCESS != read_msg (sock, read_buf)) {
      return (ST_FAILURE);
    }

    if (7 != sscanf (read_buf, SEND_JOB_TMPL, password, &id, &idx, hash, alph, &task.from, &task.to)) {
      return (ST_FAILURE);
    }    

    password[strlen (password)] = '\0';
    hash[strlen (hash)] = '\0'; 
    alph[strlen (alph)] = '\0';

    
    context->config->alph = alph;
    context->config->hash = hash;
    strcpy (task.password, password);
    
    //fprintf (stderr, "from = %d\nto = %d\n"task.from, task.to); 

    brute_iter (context, &task, check_password, &cd);
    
    sprintf (send_buf, SEND_REPORT, context->result.password, context->result.is_found, 0, 0);   
    
    if (ST_SUCCESS != send_msg (sock, send_buf)) {
      return (ST_FAILURE);
    }      
  }
  
  return (ST_SUCCESS);
}

status_t run_sync_client (context_t * context) {
  int sock;
  
  sock = socket (AF_INET, SOCK_STREAM, 0);
  if (sock < 0) {
    return (ST_FAILURE);
  }
    
  status_t status = run_sync_client_socket (context, sock);

  close (sock);
  return (status);
}

///------------------------------------------------------

void * async_client_reciever (void * arg) {
  context_t * context = arg;

  char read_buf[sizeof (SEND_REPORT) + 256];
  
  for (;;) {
    task_t task;    

    int received_is_found;
    password_t received_password;

    if (ST_FAILURE == read_msg (connected_socket, read_buf)) {
      fprintf (stderr, "read fault\n");
    } 
    int id, idx;
    if (4 != sscanf (read_buf, SEND_REPORT, received_password, &received_is_found, &id, &idx)) {
      fprintf (stderr, "parsing fault\n");
    }  

    received_password[strlen (received_password)] = '\0';

    if (received_is_found) {
      context->result.is_found = received_is_found;
      strcpy(context->result.password, received_password);
    }
  } 
}

void * async_client (void * arg) {
  context_t * context = arg;
  int connected_socket = context->connected_socket;
  sem_post (&context->lock_start);
  registry_t registry;
  registri_init (&registry);
   
  char send_buf[sizeof (SEND_JOB_TMPL) + 256];
  
  for (;;) {
    task_t task;    

    queue_pop (&context->queue, &task);  
    
    sprintf (send_buf, SEND_JOB_TMPL, task.password, task.id, task.idx, context->config->hash, context->config->alph, 0, task.from);
    
    if (ST_FAILURE == send_msg (connected_socket, send_buf)) {
      fprintf (stderr, "send fault\n");
    }
  } 
}

status_t run_async_server_socket (context_t * context, int sock) {
  struct sockaddr_in clientname;

  clientname.sin_family = AF_INET;
  clientname.sin_port = htons(PORT);
  clientname.sin_addr.s_addr = INADDR_ANY;
  if(bind(sock, (struct sockaddr *) &clientname, sizeof(clientname)) < 0) {
    return (ST_FAILURE);
  }

  if (listen (sock, 1) < 0) {
    return (ST_FAILURE);
  }
  
  pthread_t ids;
  pthread_create (&ids, NULL, bruter, context);
  pthread_detach (ids);  

  socklen_t size;     
      
  sem_init (&context->lock_start, 0, 0);
      
  for(;;) {
    size = sizeof (clientname);
    context->connected_socket = accept (sock, (struct sockaddr*) &clientname, &size);
      
    pthread_create (&ids, NULL, async_client, context);
    pthread_detach (ids);
    
    sem_wait (&context->lock_start);
    
    if (context->result.is_found) {
      break;
    }
  }
  return (ST_SUCCESS);
}

void * async_server (void * arg) {
  context_t * context = arg;
    
  int sock;
  sock = socket (AF_INET, SOCK_STREAM, 0);
  if (sock < 0) {
    return (NULL);
  }

  run_async_server_socket (context, sock);
    
  close (sock);
  return (NULL);
}

status_t run_async_server (context_t * context) {
  queue_init (&context->queue);
  
  pthread_t ids;
  pthread_create (&ids, NULL, async_server, context);
  pthread_detach (ids);

  for(;;) {    
    if (context->result.is_found) {
      break;
    }
  }
    
  return (ST_SUCCESS);
}

void * async_client_reader (void * arg) {
  context_t * context = arg;
  
  return (NULL);
}

void * async_client_sender (void * arg) {
  context_t * context = arg;
  
  return (NULL);
}

status_t run_async_client_socket (context_t * context) {

  return (ST_SUCCESS);
}

status_t run_async_client (context_t * context) {
  
}

status_t read_params (config_t * config, int argc, char * argv[]) {
  for (;;) {
    int c = getopt(argc, argv, "ria:h:n:moscel");
    if (c == -1)
      break;
    switch (c) {
    case 'r':
      config->brute_mode = BM_REC;
      break;
    case 'i':
      config->brute_mode = BM_ITER;
      break;
    case 'a':
      config->alph = optarg;
      break;
    case 'h':
      config->hash = optarg;
      break;
    case 'n':
      config->password_length = atoi (optarg);
      break;
    case 'm':
      config->run_mode = RM_MULTI;
      break;
    case 'o':
      config->run_mode = RM_SINGLE;
      break;
    case 's':
      config->run_mode = RM_SYNC_SERVER;
      break;
    case 'c':
      config->run_mode = RM_SYNC_CLIENT;
      break;
    case 'l':
      config->run_mode = RM_ASYNC_CLIENT;
      break;
    case 'e':
      config->run_mode = RM_ASYNC_CLIENT;
      break;
    default:
      return (ST_FAILURE);
    }
  }
  return (ST_SUCCESS);   
}

int main (int argc, char * argv[]) {
  config_t config = {
    .alph = ALPH,
    .password_length = MAX_LEN,
    .brute_mode = BM_ITER,
    .hash = HASH, 
    .run_mode = RM_SINGLE,
  };

  if (ST_SUCCESS != read_params(&config, argc, argv)) {
    fprintf (stderr, "Failed to parse params\n");
    return (EXIT_FAILURE);
  }

  context_t context = {
    .config = &config,
    .tasks_in_progress = 0,
    .alph_length = strlen (config.alph),
    .result = {
      .password = "1",
      .is_found = false,
    },
  };

  switch (context.config->run_mode)
    {
    case RM_MULTI:
      run_multi (&context);
      break;

    case RM_SINGLE:
      brute_all (&context, check_password, 0);
      break;

    case RM_SYNC_SERVER:
      run_sync_server (&context);
      break;
    
    case RM_SYNC_CLIENT:
      run_sync_client (&context);
      break;
    
    case RM_ASYNC_SERVER:
      run_async_server (&context);
      break;
    
    case RM_ASYNC_CLIENT:
      run_async_client (&context);
      break;
    }

  if (context.result.is_found) {
    printf ("Your password is '%s'\n", context.result.password);
  } else {
    printf ("Password is not found\n");
  }
  return (EXIT_SUCCESS);
}
